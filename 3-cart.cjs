const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]

/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/
function allProducts(products) {
    products = Object.entries(products[0])
    let result = products.reduce((arrayOfproducts, product) => {

        if (product[0] === 'utensils') {
            product[1] = product[1].map(product => {
                return Object.entries(product).flat(1)
            })
            // arrayOfproducts.push(...Object.entries(product[1]))
            arrayOfproducts.push(...product[1])
        } else {
            arrayOfproducts.push([product[0], product[1]])
        }
        return arrayOfproducts
    }, [])
    return result
}

function applyFromEntries(array) {
    return Object.fromEntries(array)
}

const arrayOfAllProducts = allProducts(products)

// Find all the items with price more than $65.

const itemsWithPriceMoreThan65 = arrayOfAllProducts
    .filter(product => {

        let price = +product[1].price.slice(1)
        return price > 65
    })

// console.log(applyFromEntries(itemsWithPriceMoreThan65))

const itemsWhereQuantityOrderedMoreThanOne = arrayOfAllProducts
    .filter(product => {

        return product[1].quantity > 1
    })

// console.log(applyFromEntries(itemsWhereQuantityOrderedMoreThanOne))

const itemsMentionedAsFragile = arrayOfAllProducts
    .filter(product => {
        return product[1].type === 'fragile'
    })

// console.log(applyFromEntries(itemsMentionedAsFragile))

const leastAndMostExpensiveItemForSingleQuantity = arrayOfAllProducts
    .map((product, index) => {
        return [index, product[1].quantity, +product[1].price.slice(1)]
    })
    .filter(product => {
        return product[1] === 1
    })
    .sort((prod1, prod2) => {
        return prod1[2] - prod2[2]
    })
    .reduce((output, product, index, array) => {
        if (index === 0) {

            output[arrayOfAllProducts[product[0]][0]] = arrayOfAllProducts[product[0]][1]
        }
        else if (index === array.length - 1) {
            output[arrayOfAllProducts[product[0]][0]] = arrayOfAllProducts[product[0]][1]

        }
        return output
    }, {})

// console.log(leastAndMostExpensiveItemForSingleQuantity)

const groupItemsBasedOnTheirStateOfMatter = arrayOfAllProducts
    .map(product => {
        if (product[0] === 'shampoo' || product[0] === "Hair-oil") {
            return ['liquid', product]

        } else {

            return ['solid', product]
        }

    })
    .reduce((output, product) => {
        output[product[0]].push(applyFromEntries([product[1]]))
        return output
    }, {
        solid: [],
        liquid: [],
        gas: []
    })
// console.log(groupItemsBasedOnTheirStateOfMatter)



//promplems done using functions



// function itemsWithPriceMoreThan65(products) {
//     products = allProducts(products)

//     let result = products
//         .filter(product => {

//             let price = +product[1].price.slice(1)
//             return price > 65
//         })
//     result = Object.fromEntries(result)
//     return result
// }

// // console.log(itemsWithPriceMoreThan65(products))

// function itemsWhereQuantityOrderedMoreThanOne(products) {
//     products = allProducts(products)

//     let result = products
//         .filter(product => {


//             return product[1].quantity > 1
//         })
//     result = Object.fromEntries(result)
//     return result
// }

// // console.log(itemsWhereQuantityOrderedMoreThanOne(products))



// function itemsMentionedAsFragile(products) {
//     products = allProducts(products)

//     let result = products
//         .filter(product => {


//             return product[1].type === 'fragile'
//         })
//     result = Object.fromEntries(result)
//     return result

// }
// // console.log(itemsMentionedAsFragile(products))

// function leastAndMostExpensiveItemForSingleQuantity(products) {
//     products = allProducts(products)

//     let result = products.filter(product => {
//         return product[1].quantity == 1
//     })
//         .sort((product1, product2) => {
//             let price1 = + product1[1].price.slice(1)
//             let price2 = + product2[1].price.slice(1)

//             return price1 - price2
//         })
//     let leatExpensiveItem = result[0]
//     let mostExpensiveItem = result[result.length - 1]
//     return Object.fromEntries([leatExpensiveItem, mostExpensiveItem])


// }

// // console.log(leastAndMostExpensiveItemForSingleQuantity(products))

// function groupItemsBasedOnTheirStateOfMatter(products) {
//     products = allProducts(products)
//     let stateOfMatter = {
//         solid: [],
//         liquid: [],
//         gas: []
//     }
//     let result = products.reduce((output, product) => {
//         if (product[0] === 'shampoo' || product[0] === "Hair-oil") {
//             product = Object.fromEntries([product])
//             stateOfMatter['liquid'].push(product)
//         } else {
//             product = Object.fromEntries([product])
//             stateOfMatter['solid'].push(product)
//         }
//         return output
//     }, stateOfMatter)
//     return result
// }
// console.log(groupItemsBasedOnTheirStateOfMatter(products))